import urllib
import re
import os

def getHtml(url):
    page = urllib.urlopen(url)
    html = page.read()
    return html

def getImg(html):
    reg = r'href="uploaded/(.*\.png)" rel'
    imgre = re.compile(reg)
    imglist = re.findall(imgre,html)
    return imglist
    
def saveImg(imglist):
    x = 0
    for imgurl in imglist:
        print x
        fs = imgurl.split('/')
        fs.remove('standardtest')
        
        path_dir = '/'.join(fs[:-1])
        path_img = '/'.join(fs)

        if not os.path.exists(path_dir):
            os.makedirs(path_dir)
        
        urllib.urlretrieve('http://www.alphamatting.com/uploaded/'+imgurl,path_img)
        x+=1
   
html = getHtml("http://www.alphamatting.com/eval_25.php")
imglist = getImg(html)
print imglist

saveImg(imglist)
